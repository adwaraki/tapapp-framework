define(
  [
    'backbone',
    'underscore',
    '/tapapp/web/js/models/SimpleModel.js'
    ], function(Backbone, _, SimpleModel) {
      var SimpleCollection = Backbone.Collection.extend({
        model : SimpleModel,
        url : '/tapapp/northbound/tapapp'
      });
      return SimpleCollection;
    });
