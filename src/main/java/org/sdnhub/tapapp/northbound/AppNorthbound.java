package org.sdnhub.tapapp.northbound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.codehaus.enunciate.jaxrs.ResponseCode;
import org.codehaus.enunciate.jaxrs.StatusCodes;
import org.codehaus.enunciate.jaxrs.TypeHint;
import org.opendaylight.controller.containermanager.IContainerManager;
import org.opendaylight.controller.northbound.commons.RestMessages;
import org.opendaylight.controller.northbound.commons.exception.ResourceNotFoundException;
import org.opendaylight.controller.northbound.commons.exception.ServiceUnavailableException;
import org.opendaylight.controller.northbound.commons.exception.UnauthorizedException;
import org.opendaylight.controller.northbound.commons.utils.NorthboundUtils;
import org.opendaylight.controller.sal.authorization.Privilege;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.utils.ServiceHelper;
import org.opendaylight.controller.sal.utils.Status;
import org.opendaylight.controller.switchmanager.ISwitchManager;
import org.sdnhub.tapapp.ITapManager;
import org.sdnhub.tapapp.TapManagerData;
import org.sdnhub.tapapp.internal.Filter;
import org.sdnhub.tapapp.internal.FilterFlowStructure;

/**
 * Northbound REST API
 * 
 * This entire web class can be accessed via /northbound prefix as specified in
 * web.xml
 * 
 * <br>
 * <br>
 * Authentication scheme : <b>HTTP Basic</b><br>
 * Authentication realm : <b>opendaylight</b><br>
 * Transport : <b>HTTP and HTTPS</b><br>
 * <br>
 * HTTPS Authentication is disabled by default.
 */
@Path("/")
public class AppNorthbound {
	@Context
	private UriInfo _uriInfo;
	private String username;

	@Context
	public void setSecurityContext(SecurityContext context) {
		if (context != null && context.getUserPrincipal() != null) {
			username = context.getUserPrincipal().getName();
		}
	}

	protected String getUserName() {
		return username;
	}

	private ISwitchManager getSwitchManagerService(String containerName) {
		IContainerManager containerManager = (IContainerManager) ServiceHelper
				.getGlobalInstance(IContainerManager.class, this);
		if (containerManager == null) {
			throw new ServiceUnavailableException("Container "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		boolean found = false;
		List<String> containerNames = containerManager.getContainerNames();
		for (String cName : containerNames) {
			if (cName.trim().equalsIgnoreCase(containerName.trim())) {
				found = true;
			}
		}
		if (found == false) {
			throw new ResourceNotFoundException(containerName + " "
					+ RestMessages.NOCONTAINER.toString());
		}
		ISwitchManager switch_manager = (ISwitchManager) ServiceHelper
				.getInstance(ISwitchManager.class, containerName, this);

		if (switch_manager == null) {
			throw new ServiceUnavailableException("Switch Manager "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		return switch_manager;
	}

	/**
	 * 
	 * Sample GET REST API call
	 * 
	 * @return A response string
	 * 
	 *         <pre>
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/app/northbound/tapapp
	 * 
	 * Response body in XML:
	 * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
	 * Sample Northbound API
	 * 
	 * Response body in JSON:
	 * Sample Northbound API
	 * </pre>
	 */
	@Path("/tapapp")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@StatusCodes()
	public List<TapManagerData> getData() {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.WRITE, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getInstance(
				ITapManager.class, "default", this);
		// ILearningSwitch simple = (ILearningSwitch)
		// ServiceHelper.getInstance(ILearningSwitch.class, "default", this);
		// getGlobalInstance(ILearningSwitch.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		Map<UUID, TapManagerData> sDataMap = simple.readData();
		if (sDataMap != null) {
			return new ArrayList<TapManagerData>(sDataMap.values());
		}
		return new ArrayList<TapManagerData>();
	}

	@Path("/tapapp/{uuid}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@TypeHint(TapManagerData.class)
	@StatusCodes()
	public TapManagerData getData(@PathParam("uuid") String uuid) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.WRITE, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		return simple.readData(UUID.fromString(uuid));
	}

	/**
	 * 
	 * Sample POST REST API call
	 * 
	 * @return A response string
	 * 
	 *         <pre>
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/app/northbound/tapapp
	 * 
	 * Response body in XML:
	 * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
	 * Sample Northbound API
	 * 
	 * Response body in JSON:
	 * Sample Northbound API
	 * </pre>
	 */
	@Path("/tapapp")
	@POST
	@StatusCodes({
			@ResponseCode(code = 201, condition = "Data Inserted successfully"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 500, condition = "Error inserting data"),
			@ResponseCode(code = 503, condition = "One or more of service is unavailable") })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createData(
			@TypeHint(TapManagerData.class) TapManagerData data) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.WRITE, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		UUID uuid = simple.createData(data);
		if (uuid == null) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.build();
		}
		return Response
				.status(Response.Status.CREATED)
				.header("Location",
						String.format("%s/%s", _uriInfo.getAbsolutePath()
								.toString(), uuid.toString()))
				.entity(uuid.toString()).build();
	}

	/**
	 * 
	 * Sample PUT REST API call
	 * 
	 * @return A response string
	 * 
	 *         <pre>
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/app/northbound/tapapp/{uuid}
	 * 
	 * Response body in XML:
	 * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
	 * Sample Northbound API
	 * 
	 * Response body in JSON:
	 * Sample Northbound API
	 * </pre>
	 */
	@Path("/tapapp/{uuid}")
	@PUT
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Data Updated successfully"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 500, condition = "Error updating data"),
			@ResponseCode(code = 503, condition = "One or more of service is unavailable") })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response updateData(@PathParam("uuid") String uuid,
			@TypeHint(TapManagerData.class) TapManagerData data) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.WRITE, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		Status status = simple.updateData(UUID.fromString(uuid), data);
		if (!status.isSuccess()) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.build();
		}
		return Response.status(Response.Status.OK).build();
	}

	/**
	 * 
	 * Sample Delete REST API call
	 * 
	 * @return A response string
	 * 
	 *         <pre>
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/app/northbound/tapapp/{uuid}
	 * 
	 * Response body in XML:
	 * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
	 * Sample Northbound API
	 * 
	 * Response body in JSON:
	 * Sample Northbound API
	 * </pre>
	 */
	@Path("/tapapp/{uuid}")
	@DELETE
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Data Deleted successfully"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 500, condition = "Error deleting data"),
			@ResponseCode(code = 503, condition = "One or more of service is unavailable") })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response updateData(@PathParam("uuid") String uuid) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.WRITE, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		Status status = simple.deleteData(UUID.fromString(uuid));
		if (!status.isSuccess()) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.build();
		}
		return Response.status(Response.Status.OK).build();
	}

	/**
	 * 
	 * POST REST API call for creating a filter
	 * 
	 * @return A response string
	 * 
	 *         <pre>
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/tapapp/northbound/tapapp/createtap
	 * 
	 * Response body in XML:
	 * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
	 * Sample Northbound API
	 * 
	 * Response body in JSON:
	 * Sample Northbound API
	 * </pre>
	 */
	@Path("/tapapp/createtap")
	@POST
	@StatusCodes({
			@ResponseCode(code = 201, condition = "Data Inserted successfully"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 500, condition = "Error inserting data"),
			@ResponseCode(code = 503, condition = "One or more of service is unavailable") })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createTap() {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.WRITE, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		String filterID = "ABC123";
		HashMap<String, Object> filterFields = new HashMap<String, Object>();
		filterFields.put("dl_dst", "00:00:00:00:00:01");
		filterFields.put("dl_src", "00:00:00:00:00:03");
		boolean createResponse = simple.createFilter(filterID, filterFields);
		if (createResponse == false) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.build();
		}
		return Response
				.status(Response.Status.CREATED)
				.header("Location",
						String.format("%s/%s", _uriInfo.getAbsolutePath()
								.toString(), String.valueOf(createResponse)))
				.entity(String.valueOf(createResponse)).build();
	}

	/**
	 * 
	 * GET REST API call for checking a filter's existence
	 * 
	 * @return A response string
	 * 
	 *         <pre>
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/app/northbound/tapapp/isfiltercreated/ABC123
	 * 
	 * Response body in XML:
	 * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
	 * Sample Northbound API
	 * 
	 * Response body in JSON:
	 * Sample Northbound API
	 * </pre>
	 */
	@Path("/tapapp/isfiltercreated/{filterID}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@TypeHint(TapManagerData.class)
	@StatusCodes()
	public Response isFilterCreated(@PathParam("filterID") String filterID) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.WRITE, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		boolean isFilterCreated = simple.isFilterCreated(filterID);
		return Response
				.status(Response.Status.CREATED)
				.header("Location",
						String.format("%s/%s", _uriInfo.getAbsolutePath()
								.toString(), String.valueOf(isFilterCreated)))
				.entity(String.valueOf(isFilterCreated)).build();
	}

	/**
	 * Returns a list of taps configured on the tap manager
	 * 
	 * @param containerName
	 *            Name of the Container (Eg. 'default')
	 * @return List of configured taps
	 * 
	 *         <pre>
	 * 
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/tapapp/northbound/tapapp/listtaps
	 * 
	 * Response body in JSON:
	 * 
	 * [
	 *     {
	 *         "filterID": "ABC123",
	 *         "filterGroupID": null,
	 *         "filterName": "default",
	 *         "filterStatus": "DISABLED",
	 *         "filterFields": {
	 *             "dl_src": "00:00:00:00:00:03",
	 *             "dl_dst": "00:00:00:00:00:01"
	 *         },
	 *         "testNode": null
	 *     }
	 * ]
	 * </pre>
	 */
	@Path("/tapapp/listtaps")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@TypeHint(Filter.class)
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Operation successful"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 404, condition = "The containerName is not found"),
			@ResponseCode(code = 503, condition = "One or more of Controller Services are unavailable") })
	public ArrayList<Filter> listTaps() {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.READ, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		ArrayList<Filter> filterList = simple.listFilters();
		return filterList;
	}

	/**
	 * Returns a list of node connectors that are up on the switch
	 * 
	 * @param containerName
	 *            Name of the Container (Eg. 'default')
	 * @return List of up node connectors
	 * 
	 *         <pre>
	 * 
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/tapapp/northbound/tapapp/default/node/OF/00:00:00:00:00:00:00:01/activeports
	 * 
	 * Response body in JSON:
	 * 
	 * </pre>
	 */
	@Path("/tapapp/{containerName}/node/{nodeType}/{nodeId}/activeports")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@TypeHint(Filter.class)
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Operation successful"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 404, condition = "The containerName is not found"),
			@ResponseCode(code = 503, condition = "One or more of Controller Services are unavailable") })
	public Set<NodeConnector> activePorts(
			@PathParam("containerName") String containerName,
			@PathParam("nodeType") String nodeType,
			@PathParam("nodeId") String nodeId) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.READ, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		ISwitchManager sManager = getSwitchManagerService(containerName);
		// ISwitchManager sManager = (ISwitchManager) ServiceHelper.getInstance(
		// ISwitchManager.class, containerName, this);
		if (sManager == null) {
			throw new ServiceUnavailableException("Switch Manager "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		Node incoming_node = Node.fromString(nodeType, nodeId);
		Set<NodeConnector> nodeConnectors = sManager
				.getUpNodeConnectors(incoming_node);
		return nodeConnectors;
	}

	/**
	 * Returns a list of flows attached to a tap
	 * 
	 * @param containerName
	 *            Name of the Container (Eg. 'default')
	 * @return List of up node connectors
	 * 
	 *         <pre>
	 * 
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/tapapp/northbound/tapapp/default/ABC123/tapflows
	 * 
	 * Response body in JSON:
	 * 
	 * </pre>
	 */
	@Path("/tapapp/{containerName}/{tapId}/tapflows")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@TypeHint(Filter.class)
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Operation successful"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 404, condition = "The containerName is not found"),
			@ResponseCode(code = 503, condition = "One or more of Controller Services are unavailable") })
	public HashSet<FilterFlowStructure> listTapFlows(
			@PathParam("containerName") String containerName,
			@PathParam("tapId") String tapId) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.READ, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}

		return (simple.tapListFlows(tapId));
	}

	/**
	 * REST API to add/delete a source from a tap
	 * 
	 * @param containerName
	 *            Name of the Container (Eg. 'default')
	 * @return List of up node connectors
	 * 
	 *         <pre>
	 * 
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/tapapp/northbound/tapapp/default/ABC123/node/OF/00:00:00:00:00:00:00:01/port/OF/1/addsource
	 * 
	 * Response body in JSON:
	 * 
	 * </pre>
	 */
	@Path("/tapapp/{containerName}/{tapId}/node/{nodeType}/{nodeId}/port/{portType}/{portId}/addsource")
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@TypeHint(Filter.class)
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Operation successful"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 404, condition = "The containerName is not found"),
			@ResponseCode(code = 503, condition = "One or more of Controller Services are unavailable") })
	public boolean tapAddSources(
			@PathParam("containerName") String containerName,
			@PathParam("tapId") String tapId,
			@PathParam("nodeType") String nodeType,
			@PathParam("nodeId") String nodeId,
			@PathParam("portType") String portType,
			@PathParam("portId") String portId) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.READ, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		ISwitchManager sManager = getSwitchManagerService(containerName);
		// ISwitchManager sManager = (ISwitchManager) ServiceHelper.getInstance(
		// ISwitchManager.class, containerName, this);
		if (sManager == null) {
			throw new ServiceUnavailableException("Switch Manager "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		Node srcNode = Node.fromString(nodeType, nodeId);
		NodeConnector srcPort = NodeConnector.fromStringNoNode(portType,
				portId, srcNode);
		ImmutablePair<Node, NodeConnector> srcPair = new ImmutablePair<Node, NodeConnector>(
				srcNode, srcPort);
		HashSet<ImmutablePair<Node, NodeConnector>> srcList = new HashSet<ImmutablePair<Node, NodeConnector>>();
		srcList.add(srcPair);
		return (simple.tapAddSources(tapId, srcList));
	}

	/**
	 * REST API to add/delete a source from a tap
	 * 
	 * @param containerName
	 *            Name of the Container (Eg. 'default')
	 * @return List of up node connectors
	 * 
	 *         <pre>
	 * 
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/tapapp/northbound/tapapp/default/ABC123/node/OF/00:00:00:00:00:00:00:01/port/OF/2
	 * 
	 * Response body in JSON:
	 * 
	 * </pre>
	 */
	@Path("/tapapp/{containerName}/{tapId}/node/{nodeType}/{nodeId}/port/{portType}/{portId}/addsink")
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@TypeHint(Filter.class)
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Operation successful"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 404, condition = "The containerName is not found"),
			@ResponseCode(code = 503, condition = "One or more of Controller Services are unavailable") })
	public boolean tapAddSinks(
			@PathParam("containerName") String containerName,
			@PathParam("tapId") String tapId,
			@PathParam("nodeType") String nodeType,
			@PathParam("nodeId") String nodeId,
			@PathParam("portType") String portType,
			@PathParam("portId") String portId) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.READ, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		ISwitchManager sManager = getSwitchManagerService(containerName);
		// ISwitchManager sManager = (ISwitchManager) ServiceHelper.getInstance(
		// ISwitchManager.class, containerName, this);
		if (sManager == null) {
			throw new ServiceUnavailableException("Switch Manager "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		Node sinkNode = Node.fromString(nodeType, nodeId);
		NodeConnector sinkPort = NodeConnector.fromStringNoNode(portType,
				portId, sinkNode);
		ImmutablePair<Node, NodeConnector> sinkPair = new ImmutablePair<Node, NodeConnector>(
				sinkNode, sinkPort);
		HashSet<ImmutablePair<Node, NodeConnector>> sinkList = new HashSet<ImmutablePair<Node, NodeConnector>>();
		sinkList.add(sinkPair);
		return (simple.tapAddSinks(tapId, sinkList));
	}

	/**
	 * REST API to activate a tap
	 * 
	 * @param containerName
	 *            Name of the Container (Eg. 'default')
	 * @return true or false depending on activation state
	 * 
	 *         <pre>
	 * 
	 * Example:
	 * 
	 * Request URL:
	 * http://localhost:8080/tapapp/northbound/tapapp/default/ABC123/activatetap
	 * 
	 * Response body in JSON:
	 * 
	 * </pre>
	 */
	@Path("/tapapp/{containerName}/{tapId}/activatetap")
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@TypeHint(Filter.class)
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Operation successful"),
			@ResponseCode(code = 401, condition = "User not authorized to perform this operation"),
			@ResponseCode(code = 404, condition = "The containerName is not found"),
			@ResponseCode(code = 503, condition = "One or more of Controller Services are unavailable") })
	public boolean activateTap(
			@PathParam("containerName") String containerName,
			@PathParam("tapId") String tapId) {
		if (!NorthboundUtils.isAuthorized(getUserName(), "default",
				Privilege.READ, this)) {
			throw new UnauthorizedException(
					"User is not authorized to perform this operation");
		}
		ITapManager simple = (ITapManager) ServiceHelper.getGlobalInstance(
				ITapManager.class, this);
		if (simple == null) {
			throw new ServiceUnavailableException("Simple Service "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		ISwitchManager sManager = getSwitchManagerService(containerName);
		// ISwitchManager sManager = (ISwitchManager) ServiceHelper.getInstance(
		// ISwitchManager.class, containerName, this);
		if (sManager == null) {
			throw new ServiceUnavailableException("Switch Manager "
					+ RestMessages.SERVICEUNAVAILABLE.toString());
		}
		return (simple.activateTap(tapId));
	}
}
