package org.sdnhub.tapapp.internal;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.opendaylight.controller.sal.action.Action;
import org.opendaylight.controller.sal.action.Output;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.flowprogrammer.Flow;
import org.opendaylight.controller.sal.flowprogrammer.IFlowProgrammerService;
import org.opendaylight.controller.sal.match.Match;
import org.opendaylight.controller.sal.match.MatchType;
import org.opendaylight.controller.sal.packet.IDataPacketService;
import org.opendaylight.controller.sal.utils.HexEncode;
import org.opendaylight.controller.sal.utils.Status;
import org.opendaylight.controller.sal.utils.StatusCode;
import org.opendaylight.controller.switchmanager.ISwitchManager;
import org.sdnhub.tapapp.ITapManager;
import org.sdnhub.tapapp.TapManagerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Sets;
import com.google.common.net.InetAddresses;

public class TapManager implements ITapManager, Serializable {

	private static final long serialVersionUID = -3104660995451788658L;
	protected static final Logger logger = LoggerFactory
			.getLogger(TapManager.class);

	private Map<UUID, TapManagerData> data;
	private IDataPacketService dataPacketService = null;
	private ISwitchManager switchManager = null;
	private IFlowProgrammerService flowProgrammer = null;
	private String tapManagerStatus;
	private Map<String, Filter> createdFilters;
	private Map<Long, Flow> tapManagerFlows;
	private ListMultimap<String, FilterFlowStructure> filterFlowMap;

	/**
	 * Function called by the dependency manager when all the required
	 * dependencies are satisfied in java
	 * 
	 */
	public void init() {
		this.createdFilters = new HashMap<String, Filter>();
		this.tapManagerFlows = new HashMap<Long, Flow>();
		this.filterFlowMap = ArrayListMultimap.create();
		this.data = new ConcurrentHashMap<UUID, TapManagerData>();
		this.setTapManagerStatus("ENABLED");
		logger.info("Tap Manager initialized");
	}

	void start() {
		logger.info("Tap Manager starting");
		logger.info("All taps disabled on last stop. Re-activate taps");
	}

	/**
	 * Function that is called by the bundle manager on stop 1. Deactivate all
	 * taps 2. Set status to disabled
	 */
	void stop() {
		boolean deacStatus;
		for (String filterID : this.createdFilters.keySet()) {
			deacStatus = this.deactivateTap(filterID);
			if (deacStatus == false) {
				logger.error("ERROR: Could not stop Tap Manager");
				break;
			} else {
				continue;
			}
		}
		this.tapManagerStatus = "DISABLED";
		logger.info("Tap Manager stopping");
	}

	void setDataPacketService(IDataPacketService s) {
		this.dataPacketService = s;
	}

	void unsetDataPacketService(IDataPacketService s) {
		if (this.dataPacketService == s) {
			this.dataPacketService = null;
		}
	}

	/*
	 * Function that sets the flow programmer service
	 */
	public void setFlowProgrammerService(IFlowProgrammerService flowService) {
		this.flowProgrammer = flowService;
	}

	/*
	 * Function that unsets the flow programmer service
	 */
	public void unsetFlowProgrammerService(IFlowProgrammerService flowService) {
		if (this.flowProgrammer == flowService) {
			this.flowProgrammer = null;
		}
	}

	/*
	 * Function to set the switch manager
	 */
	void setSwitchManager(ISwitchManager swManager) {
		logger.debug("SwitchManager set");
		this.switchManager = swManager;
	}

	/*
	 * Function to unset the switch manager
	 */
	void unsetSwitchManager(ISwitchManager swManager) {
		if (this.switchManager == swManager) {
			logger.debug("SwitchManager removed!");
			this.switchManager = null;
		}
	}

	/**
	 * Function to get the Tap Manager status
	 * 
	 * @return
	 */
	public String getTapManagerStatus() {
		return tapManagerStatus;
	}

	/**
	 * Function to set the tap manager status
	 * 
	 * @param tapManagerStatus
	 */
	public void setTapManagerStatus(String tapManagerStatus) {
		this.tapManagerStatus = tapManagerStatus;
	}

	/**
	 * Function to create a tap/filter (internally calls
	 * Filter.initializeFilter)
	 */
	@Override
	public boolean createFilter(String filterID,
			HashMap<String, Object> newFilterFields) {
		Filter newFilter = new Filter(filterID);
		if (newFilterFields.isEmpty()) {
			logger.warn("WARNING : Empty filter created. Setup filter fields");
			newFilter.setFilterStatus("DISABLED");
			createdFilters.put(filterID, newFilter);
			return (true);
		} else {
			if (newFilter.initializeFilter(filterID, newFilterFields) != null) {
				newFilter.setFilterStatus("DISABLED");
				createFlows(newFilter);
				logger.info("Filter created but disabled. Activate filter to setup flows.");
				createdFilters.put(filterID, newFilter);
				return (true);
			} else {
				logger.error("ERROR : Could not create filter {}", filterID);
				return (false);
			}
		}
	}

	/*
	 * Function to delete a tap/filter
	 */
	public boolean deleteFilter(String filterID) {
		Filter currentFilter = this.createdFilters.get(filterID);
		logger.warn("Removing all filter information and associated flows");
		logger.info("Removing flows associated with this filter");
		if (this.deleteFlows(currentFilter)) {
			logger.info("Removing filter from list of managed filters");
			currentFilter.setFilterStatus("DISABLED");
			this.createdFilters.remove(filterID);
			return (true);
		} else {
			logger.warn("Error occurred while deleting filter. Try again.");
			return (false);
		}
	}
	
	/*
	 * Function to activate a filter, this will install flows on the switch
	 */
	@Override
	public boolean activateTap(String filterID) {
		Status flowPgmStatus;
		Node source;
		long sourceID = 0;
		long sinkID = 0;
		ArrayList<Action> tapActionList = new ArrayList<Action>();
		Filter currentFilter = this.createdFilters.get(filterID);
		HashMap<String, Object> filterFields = (HashMap<String, Object>) currentFilter
				.getFilterFields();
		Collection<FilterFlowStructure> configuredFlows = (Collection<FilterFlowStructure>) this.filterFlowMap
				.get(filterID);
		@SuppressWarnings("unchecked")
		HashSet<ImmutablePair<Node, NodeConnector>> sinkList = (HashSet<ImmutablePair<Node, NodeConnector>>) filterFields
				.get("sinks");
		for (FilterFlowStructure flowInfo : configuredFlows) {
			Flow currentFlow = flowInfo.getFlow();
			source = flowInfo.getNodeInfo();
			for (ImmutablePair<Node, NodeConnector> sink : sinkList) {
				sourceID = (long) flowInfo.getNodeInfo().getID();
				sinkID = (long) sink.getLeft().getID();
				if (sourceID != sinkID) {
					logger.warn(
							"Source id {} and sink id {} don't match. Multiswitch configuration not "
									+ "implemented", sourceID, sinkID);
					return(false);
				} else {
					tapActionList.add(new Output(sink.getRight()));
				}
			}
			currentFlow.setActions(tapActionList);
			flowPgmStatus = createIntersectingFlows(filterID, source, currentFlow);
			if (flowPgmStatus.isSuccess()) {
				logger.trace("Flow {} corresponding to tap ID {} installed "
						+ "on switch {}", currentFlow, filterID, sourceID);
				this.tapManagerFlows.put(currentFlow.getId(), currentFlow);
				flowInfo.setMappedFlowStatus("ENABLED");
			} else {
				logger.warn("Flow {} was not installed for tap {} on "
						+ "switch {}. Tap not active.", currentFlow, filterID,
						sourceID);
				return (false);
			}
		}
		currentFilter.setFilterStatus("ENABLED");
		return (true);
	}
	
	/*
	 * Function to deactivate a tap. It involves the following: 1. If tap
	 * active, remove flows from switch (flows available from filterFlowMap) 2.
	 * Set all associated flow statuses to DISABLED 3. Set tap status to
	 * DISABLED
	 */
	@Override
	public boolean deactivateTap(String filterID) {
		Status flowRemovalStatus;
		Filter currentFilter = this.createdFilters.get(filterID);
		ArrayList<FilterFlowStructure> configuredFlows = (ArrayList<FilterFlowStructure>) this.filterFlowMap
				.get(filterID);
		for (FilterFlowStructure flowInfo : configuredFlows) {
			Node activeNode = (Node) flowInfo.getNodeInfo();
			Flow associatedFlow = flowInfo.getFlow();
			flowRemovalStatus = this.flowProgrammer.removeFlow(activeNode,
					associatedFlow);
			if (flowRemovalStatus.isSuccess()) {
				logger.trace("Flow {} corresponding to tap ID {} removed "
						+ "from switch {}", associatedFlow, filterID,
						activeNode.getID());
				flowInfo.setMappedFlowStatus("DISABLED");
			} else {
				logger.warn("Flow {} was not removed for tap {} on "
						+ "switch {}. Tap still active.", associatedFlow,
						filterID, activeNode);
				return (false);
			}
		}
		logger.info("All flows removed successfully. Deactivating tap {}",
				filterID);
		currentFilter.setFilterStatus("DISABLED");
		return (true);
	}

	/**
	 * Function to list all the created filters on the node
	 */
	public ArrayList<Filter> listCreatedFilters() {
		if (!this.createdFilters.isEmpty()) {
			logger.debug("Listing created filters. Check for possible typecast issues");
			return ((ArrayList<Filter>) this.createdFilters.values());
		} else {
			logger.info("No filters have been created");
			return null;
		}
	}

	/**
	 * Function to check if filter exists
	 */
	@Override
	public boolean isFilterCreated(String filterID) {
		return (createdFilters.containsKey(filterID));
	}

	/**
	 * Function to create the TapManager data information
	 */
	@Override
	public UUID createData(TapManagerData datum) {
		UUID uuid = UUID.randomUUID();
		TapManagerData sData = new TapManagerData(uuid.toString(),
				datum.getFoo(), datum.getBar());
		data.put(uuid, sData);
		return uuid;
	}
	
	/**
	 * Function to add a source to a tap
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean tapAddSources(String filterID, 
			HashSet<ImmutablePair<Node, NodeConnector>> srcList) {
		//TODO - Check the source count and flow count on the filter. Both
		// have to match, at the very least, not including intersecting flows recorded
		Filter currentFilter = (Filter) createdFilters.get(filterID);
		HashMap<String, Object> filterFields = currentFilter.getFilterFields();
		HashSet<ImmutablePair<Node, NodeConnector>> existingSources = 
				(HashSet<ImmutablePair<Node, NodeConnector>>) filterFields.get("sources");
		if(existingSources == null) {
			boolean addStatus = currentFilter.tapAddSrcSinkInfo(srcList, "sources");
			if(addStatus) {
				return(modifyFlows(currentFilter, srcList));
			}
			else {
				return(false);
			}
			
		}
		else {
			HashSet<ImmutablePair<Node, NodeConnector>> newSources = 
					new HashSet<ImmutablePair<Node, NodeConnector>>();
			Sets.symmetricDifference(existingSources, srcList).copyInto(newSources);
			boolean addStatus = currentFilter.tapAddSrcSinkInfo(newSources, "sources");
			if(addStatus) {
				return(modifyFlows(currentFilter, srcList));
			}
			else {
				return(false);
			}
		}
	}
	
	/**
	 * Function to add sinks to a tap
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean tapAddSinks(String filterID,
			HashSet<ImmutablePair<Node, NodeConnector>> sinkList) {
		Filter currentFilter = (Filter) createdFilters.get(filterID);
		HashMap<String, Object> filterFields = currentFilter.getFilterFields();
		HashSet<ImmutablePair<Node, NodeConnector>> existingSinks = 
				(HashSet<ImmutablePair<Node, NodeConnector>>) filterFields.get("sinks");
		if(existingSinks == null) {
			return(currentFilter.tapAddSrcSinkInfo(sinkList, "sinks"));
			
		}
		else {
			HashSet<ImmutablePair<Node, NodeConnector>> newSinks = 
					new HashSet<ImmutablePair<Node, NodeConnector>>();
			Sets.symmetricDifference(existingSinks, sinkList).copyInto(newSinks);
			return(currentFilter.tapAddSrcSinkInfo(newSinks, "sinks"));
		}
	}
	
	/**
	 * Function to remove sources from a tap
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean tapRemoveSources(String filterID,
			HashSet<ImmutablePair<Node, NodeConnector>> sourceList) {
		Filter currentFilter = (Filter) createdFilters.get(filterID);
		HashMap<String, Object> filterFields = currentFilter.getFilterFields();
		HashSet<ImmutablePair<Node, NodeConnector>> existingSources = 
				(HashSet<ImmutablePair<Node, NodeConnector>>) filterFields.get("sources");
		if(existingSources == null) {
			boolean removeStatus = currentFilter.tapRemoveSrcSinkInfo(sourceList, "sources"); 
			if(removeStatus) {
				return(modifyFlows(currentFilter, sourceList));
			}
			else {
				return(false);
			}
			
		}
		else {
			HashSet<ImmutablePair<Node, NodeConnector>> newSources = 
					new HashSet<ImmutablePair<Node, NodeConnector>>();
			Sets.difference(existingSources, sourceList).copyInto(newSources);
			boolean removeStatus = currentFilter.tapRemoveSrcSinkInfo(newSources, "sources"); 
			if(removeStatus) {
				return(modifyFlows(currentFilter, sourceList));
			}
			else {
				return(false);
			}
		}
	}
	
	/**
	 * Function to remove sinks from a tap
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean tapRemoveSinks(String filterID,
			HashSet<ImmutablePair<Node, NodeConnector>> sinkList) {
		Filter currentFilter = (Filter) createdFilters.get(filterID);
		HashMap<String, Object> filterFields = currentFilter.getFilterFields();
		HashSet<ImmutablePair<Node, NodeConnector>> existingSinks = 
				(HashSet<ImmutablePair<Node, NodeConnector>>) filterFields.get("sinks");
		if(existingSinks == null) {
			return(currentFilter.tapRemoveSrcSinkInfo(sinkList, "sinks"));
			
		}
		else {
			HashSet<ImmutablePair<Node, NodeConnector>> newSinks = 
					new HashSet<ImmutablePair<Node, NodeConnector>>();
			Sets.difference(existingSinks, sinkList).copyInto(newSinks);
			return(currentFilter.tapRemoveSrcSinkInfo(newSinks, "sinks"));
		}
	}

	/**
	 * Function to read TapManager data
	 */
	@Override
	public TapManagerData readData(UUID uuid) {
		return data.get(uuid);
	}

	/**
	 * Over ridden function for read data
	 */
	@Override
	public Map<UUID, TapManagerData> readData() {
		return data;
	}

	/**
	 * Function to update TapManager data
	 */
	@Override
	public Status updateData(UUID uuid, TapManagerData datum) {
		data.put(uuid, datum);
		return new Status(StatusCode.SUCCESS);
	}

	/**
	 * Function to delete TapManager data
	 */
	@Override
	public Status deleteData(UUID uuid) {
		data.remove(uuid);
		return new Status(StatusCode.SUCCESS);
	}
	
	/**
	 * Function to list all the taps on the tap manager
	 */
	@Override
	public ArrayList<Filter> listFilters() {
		return (new ArrayList<Filter>(createdFilters.values()));
	}
	
	/**
	 * Function to list all the flows attached to a tap
	 * @return HashSet of FilterFlowStructures
	 */
	@Override
	public HashSet<FilterFlowStructure> tapListFlows(String filterID) {
		HashSet<FilterFlowStructure> attachedFlows = 
				new HashSet<FilterFlowStructure>();
		attachedFlows.addAll(this.filterFlowMap.get(filterID));
		return(attachedFlows);
	}
	
	/*
	 * Function to generate a random long value to use as the flow ID
	 */
	private long generateFlowID() {
		return ((new Random()).nextLong());
	}

	/*
	 * Function to program the flow associated with the filter on the
	 * switchArrayListMultiMap
	 */
	private void createFlows(Filter filter) {
		// Add all the filter fields as match data on the rule set
		Match match = new Match();
		HashMap<String, Object> filterFields = (HashMap<String, Object>) filter
				.getFilterFields();
		FilterFlowStructure associatedFlowStructure = new FilterFlowStructure();
		@SuppressWarnings("unchecked")
		HashSet<ImmutablePair<Node, NodeConnector>> sourceList = (HashSet<ImmutablePair<Node, NodeConnector>>) filterFields
				.get("sources");
		Set<Map.Entry<String, Object>> fieldSet = filterFields.entrySet();
		// For each source listed on the filter, create a flow structure and
		// attach the source to this.
		// In the end, return the ArrayList of FilterFlowStructure objects
		// attached to that filterID
		if (sourceList == null) {
			Flow filterSourceFlow = new Flow();
			filterSourceFlow.setId(this.generateFlowID());
			for (Map.Entry<String, Object> fieldEntry : fieldSet) {
				switch (fieldEntry.getKey()) {
				case "dl_src":
					match.setField(MatchType.DL_SRC, HexEncode.bytesFromHexString(
							(String)fieldEntry.getValue()));
					continue;
				case "dl_dst":
					match.setField(MatchType.DL_DST, HexEncode.bytesFromHexString(
							(String)fieldEntry.getValue()));
					continue;
				case "dl_vlan":
					short dl_vlan = Short.parseShort((String) fieldEntry
							.getValue());
					match.setField(MatchType.DL_VLAN, dl_vlan);
					continue;
				case "dl_vlan_outer":
					short dl_vlan_outer = Short.parseShort((String) fieldEntry
							.getValue());
					match.setField(MatchType.DL_OUTER_VLAN, dl_vlan_outer);
					continue;
				case "dl_type":
					short dl_type = Short.parseShort((String) fieldEntry
							.getValue());
					match.setField(MatchType.DL_TYPE, dl_type);
					continue;
				case "nw_dst":
					String dst_address = (String) fieldEntry.getValue();
					if (InetAddresses.isInetAddress(dst_address)) {
						try {
							InetAddress nw_dst = InetAddress
									.getByName(dst_address);
							match.setField(MatchType.NW_DST, nw_dst);
							continue;
						} catch (UnknownHostException e) {
							logger.error(
									"Unknown destination host address specified. "
											+ "Skipping filter field {}",
									dst_address);
							continue;
						}
					} else {
						logger.error("Invalid destination IP address {}. "
								+ "Skipping filter field", dst_address);
						continue;
					}
				case "nw_src":
					String src_address = (String) fieldEntry.getValue();
					if (InetAddresses.isInetAddress(src_address)) {
						try {
							InetAddress nw_src = InetAddress
									.getByName(src_address);
							match.setField(MatchType.NW_DST, nw_src);
							continue;
						} catch (UnknownHostException e) {
							logger.error(
									"Unknown destination host address specified. "
											+ "Skipping filter field {}",
									src_address);
							continue;
						}
					} else {
						logger.error("Invalid destination IP address {}. "
								+ "Skipping filter field", src_address);
						continue;
					}
				case "nw_protocol":
					byte nw_protocol = Byte.parseByte((String) fieldEntry
							.getValue());
					match.setField(MatchType.NW_PROTO, nw_protocol);
					continue;
				case "nw_tos":
					byte nw_tos = Byte
							.parseByte((String) fieldEntry.getValue());
					match.setField(MatchType.NW_TOS, nw_tos);
					continue;
				case "tp_src":
					short tp_src = Short.parseShort((String) fieldEntry
							.getValue());
					match.setField(MatchType.TP_SRC, tp_src);
					continue;
				case "tp_dst":
					short tp_dst = Short.parseShort((String) fieldEntry
							.getValue());
					match.setField(MatchType.TP_DST, tp_dst);
					continue;
				}
			}
			filterSourceFlow.setMatch(match);
			associatedFlowStructure.setFlow(filterSourceFlow);
			this.filterFlowMap.put(filter.getFilterID(),
					associatedFlowStructure);
		} else if (!sourceList.isEmpty()) {
			for (ImmutablePair<Node, NodeConnector> iter : sourceList) {
				Flow filterSourceFlow = new Flow();
				filterSourceFlow.setId(this.generateFlowID());
				for (Map.Entry<String, Object> fieldEntry : fieldSet) {
					switch (fieldEntry.getKey()) {
					case "dl_src":
						match.setField(MatchType.DL_SRC, HexEncode.bytesFromHexString(
								(String)fieldEntry.getValue()));
						continue;
					case "dl_dst":
						match.setField(MatchType.DL_DST, HexEncode.bytesFromHexString(
								(String)fieldEntry.getValue()));
						continue;
					case "dl_vlan":
						short dl_vlan = Short.parseShort((String) fieldEntry
								.getValue());
						match.setField(MatchType.DL_VLAN, dl_vlan);
						continue;
					case "dl_vlan_outer":
						short dl_vlan_outer = Short
								.parseShort((String) fieldEntry.getValue());
						match.setField(MatchType.DL_OUTER_VLAN, dl_vlan_outer);
						continue;
					case "dl_type":
						short dl_type = Short.parseShort((String) fieldEntry
								.getValue());
						match.setField(MatchType.DL_TYPE, dl_type);
						continue;
					case "nw_dst":
						String dst_address = (String) fieldEntry.getValue();
						if (InetAddresses.isInetAddress(dst_address)) {
							try {
								InetAddress nw_dst = InetAddress
										.getByName(dst_address);
								match.setField(MatchType.NW_DST, nw_dst);
								continue;
							} catch (UnknownHostException e) {
								logger.error(
										"Unknown destination host address specified. "
												+ "Skipping filter field {}",
										dst_address);
								continue;
							}
						} else {
							logger.error("Invalid destination IP address {}. "
									+ "Skipping filter field", dst_address);
							continue;
						}
					case "nw_src":
						String src_address = (String) fieldEntry.getValue();
						if (InetAddresses.isInetAddress(src_address)) {
							try {
								InetAddress nw_src = InetAddress
										.getByName(src_address);
								match.setField(MatchType.NW_DST, nw_src);
								continue;
							} catch (UnknownHostException e) {
								logger.error(
										"Unknown destination host address specified. "
												+ "Skipping filter field {}",
										src_address);
								continue;
							}
						} else {
							logger.error("Invalid destination IP address {}. "
									+ "Skipping filter field", src_address);
							continue;
						}
					case "nw_protocol":
						byte nw_protocol = Byte.parseByte((String) fieldEntry
								.getValue());
						match.setField(MatchType.NW_PROTO, nw_protocol);
						continue;
					case "nw_tos":
						byte nw_tos = Byte.parseByte((String) fieldEntry
								.getValue());
						match.setField(MatchType.NW_TOS, nw_tos);
						continue;
					case "tp_src":
						short tp_src = Short.parseShort((String) fieldEntry
								.getValue());
						match.setField(MatchType.TP_SRC, tp_src);
						continue;
					case "tp_dst":
						short tp_dst = Short.parseShort((String) fieldEntry
								.getValue());
						match.setField(MatchType.TP_DST, tp_dst);
						continue;
					}
				}
				match.setField(MatchType.IN_PORT, iter.getRight());
				filterSourceFlow.setMatch(match);
				associatedFlowStructure.setNodeInfo(iter.getLeft());
				associatedFlowStructure.setFlow(filterSourceFlow);
				this.filterFlowMap.put(filter.getFilterID(),
						associatedFlowStructure);
			}
		} else {
			logger.info("Temporarily unhandled condition");
		}
	}
	
	/**
	 * Function to modify a flow on the a tap with an added source
	 * @param filter
	 * @return true if modification was successful, false otherwise
	 */
	private boolean modifyFlows(Filter filter, HashSet<ImmutablePair<Node, NodeConnector>> srcList) {
		Collection<FilterFlowStructure> filterFlowList = 
				this.filterFlowMap.get(filter.getFilterID());
		if((filterFlowList.size() == 1) && (srcList.size() == 1)) {
			/* If this is the only flow in the list, it must have been created when
			   the filter was created without any sources. It should be safe to assume 
			   that MatchType.IN_PORT will be empty for this flow and we can add in the node
			   connector from the only entry in the hash set
			*/
			logger.info("Filter has single flow. Attaching source");
			FilterFlowStructure associatedFlow = filterFlowList.iterator().next();
			Match flowMatchSet = associatedFlow.getFlow().getMatch();
			NodeConnector srcPort = srcList.iterator().next().getRight();
			flowMatchSet.setField(MatchType.IN_PORT, srcPort);
			Node srcSwitch = srcList.iterator().next().getLeft();
			associatedFlow.setNodeInfo(srcSwitch);
		}
		else {
			FilterFlowStructure referenceFlow = null;
			/* This case should be fulfilled when adding sources and creating mapped flows for
			 * all non-intersecting flows listed on the tap
			 */
			logger.info("Mismatched flow count and source count on filter. Reprogramming.");
			ArrayDeque<ImmutablePair<Node, NodeConnector>> srcListQueue = 
					new ArrayDeque<ImmutablePair<Node, NodeConnector>>();
			srcListQueue.addAll(srcList);
			for(FilterFlowStructure currentFlow : filterFlowList) {
				ArrayList<MatchType> matchFields = 
						(ArrayList<MatchType>) currentFlow.getFlow().getMatch().getMatchesList();
				if(matchFields.contains(MatchType.IN_PORT)) {
					continue;
				}
				else {
					currentFlow.getFlow().getMatch().setField(MatchType.IN_PORT, srcListQueue.pop().getRight());;
				}
			}
			Iterator<ImmutablePair<Node, NodeConnector>> itr = srcListQueue.iterator();
			while(itr.hasNext()) {
				FilterFlowStructure clonedFlowStructure = new FilterFlowStructure(referenceFlow);
				clonedFlowStructure.getFlow().getMatch().setField(MatchType.IN_PORT, itr.next().getRight());
				itr.remove();
			}
		}
		return true;
	}

	/*
	 * Function to delete flows (called internally by deleteFilter)
	 */
	private boolean deleteFlows(Filter filter) {
		String filterID = filter.getFilterID();
		logger.info("Removing associated flows for filter {} from "
				+ " filter flow map", filterID);
		this.filterFlowMap.removeAll(filterID);
		return (true);
	}

	/**
	 * Function to check and create intersecting flows on the switch
	 * 
	 * @param source
	 *            The source node/switch on which the flows are to be programmed
	 * @param currentFlow
	 *            The current flow against which all flow match fields will be
	 *            compared
	 * @return Flow program status of SUCCESS or FAILURE
	 */
	private Status createIntersectingFlows(String filterID, Node source, Flow currentFlow) {
		Match intersectingMatch = null;
		Status flowPgmStatus = null;
		FilterFlowStructure associatedIntFlow = new FilterFlowStructure();
		Flow intersectingFlow = new Flow();
		Map<Long, Flow> tempFlowMap = new HashMap<Long, Flow>();
		Match currentFlowMatch = currentFlow.getMatch();
		ArrayList<Action> intersectingActionList = new ArrayList<Action>();
		if(!this.tapManagerFlows.isEmpty()) {
			for (Flow existingFlow : this.tapManagerFlows.values()) {
				if (currentFlowMatch.intersetcs(existingFlow.getMatch())) {
					intersectingMatch = currentFlowMatch
							.getIntersection(existingFlow.getMatch());
				}
				long flowID = this.generateFlowID();
				intersectingFlow.setId(flowID);
				intersectingFlow.setMatch(intersectingMatch);
				intersectingActionList.addAll(currentFlow.getActions());
				intersectingActionList.addAll(existingFlow.getActions());
				intersectingFlow.setActions(intersectingActionList);
				short intFlowPriority = (short) ((Math.max(
						currentFlow.getPriority(), existingFlow.getPriority())) + 1);
				intersectingFlow.setPriority(intFlowPriority);
				associatedIntFlow.setFlow(intersectingFlow);
				associatedIntFlow.setNodeInfo(source);
				associatedIntFlow.setIntersectingFlow(true);
				flowPgmStatus = this.flowProgrammer.addFlow(source,
						intersectingFlow);
				if (flowPgmStatus.isSuccess()) {
					logger.trace("Intersecting flow {} installed on switch {}",
							intersectingFlow, source.getID());
					// Cannot store information into the main flow map when it is
					// being iterated.
					// Store in a temp map and update in the end
					associatedIntFlow.setMappedFlowStatus("ENABLED");
					this.filterFlowMap.put(filterID, associatedIntFlow);
					tempFlowMap.put(intersectingFlow.getId(), intersectingFlow);
					continue;
				} else {
					logger.warn(
							"Intersecting flow {} not installed on switch {}. Tap not active.",
							intersectingFlow, source.getID());
					return (flowPgmStatus);
				}
			}
		}
		logger.info(currentFlow.toString());
		flowPgmStatus = this.flowProgrammer.addFlow(source, currentFlow);
		if (flowPgmStatus.isSuccess()) {
			logger.trace("Flow {} installed on switch {}", currentFlow,
					source.getID());
			this.tapManagerFlows.put(currentFlow.getId(), currentFlow);
			this.tapManagerFlows.putAll(tempFlowMap);
			return (flowPgmStatus);
		} else {
			logger.warn("Flow {} not installed on switch {}. Tap not active.",
					currentFlow, source.getID());
			return (flowPgmStatus);
		}
	}
}
