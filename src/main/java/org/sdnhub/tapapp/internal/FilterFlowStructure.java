package org.sdnhub.tapapp.internal;

import java.io.Serializable;

import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.flowprogrammer.Flow;

/**
 * @author adwaraki
 *
 */

public class FilterFlowStructure implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3433260125131641695L;
	private String mappedFlowStatus;
	private long switchID;
	private boolean isIntersectingFlow;
	private Node attachedNode;
	private Flow attachedFlow;
	
	public FilterFlowStructure() {
		this.mappedFlowStatus = "DISABLED";
		this.setIntersectingFlow(false);
		this.attachedNode = null;
		this.attachedFlow = null;
	}
	
	public FilterFlowStructure(Node switchInfo, Flow attachedFlow) {
		this.mappedFlowStatus = "DISABLED";
		this.setIntersectingFlow(false);
		this.attachedNode = switchInfo;
		this.attachedFlow = attachedFlow;
	}
	
	public FilterFlowStructure(FilterFlowStructure original) {
		this.attachedFlow = original.attachedFlow;
		this.attachedNode = original.attachedNode;
	}
	
	/*
	 * Function to get switch IDswitchID
	 */
	public long getSwitchID() {
		return(this.switchID);
	}
	
	/*
	 * Function to set switch ID
	 */
	public void setSwitchID(long id) {
		this.switchID = id;
	}
	
	/*
	 * Function to get node information
	 */
	public Node getNodeInfo() {
		return(this.attachedNode);
	}
	
	/*
	 * Function to set node information
	 */
	public void setNodeInfo(Node node) {
		this.attachedNode = node;
	}
	
	/*
	 * Function to get mapped flow status
	 */
	public String getMappedFlowStatus() {
		return(this.mappedFlowStatus);
	}
	
	/*
	 * Function to set mapped flow status
	 */
	public void setMappedFlowStatus(String status) {
		this.mappedFlowStatus = status;
	}
	
	/*
	 * Function to get switch ID
	 */
	public Flow getFlow() {
		return(this.attachedFlow);
	}
	
	/*
	 * Function to get switch ID
	 */
	public void setFlow(Flow attachedFlow) {
		this.attachedFlow = attachedFlow;
	}

	public boolean isIntersectingFlow() {
		return isIntersectingFlow;
	}

	public void setIntersectingFlow(boolean isIntersectingFlow) {
		this.isIntersectingFlow = isIntersectingFlow;
	}
}