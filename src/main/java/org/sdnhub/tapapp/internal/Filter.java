/**
 * 
 */
package org.sdnhub.tapapp.internal;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adwaraki
 *
 */

public class Filter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6635129565318826238L;
	private String filterID;
	private String filterGroupID;
	private String filterName;
	private String filterStatus;
	private HashMap<String, Object> filterFields;
	private static final Logger logger = LoggerFactory.getLogger(Filter.class);
	
	/**
     * Class constructor to create a filter with an empty set of fields
     */
	public Filter(String filterID) {
		// Create an empty filter object that will later be populated by createFilter
		this.filterID = filterID;
		this.filterName = "default";
		this.filterStatus = "DISABLED";
		this.filterFields = new HashMap<String, Object>();
	}
	
	/**
     * Function that returns the filter ID
     */
	public String getFilterID() {
		return(this.filterID);
	}
	
	/**
     * Function that sets the filter ID
     */
	public void setFilterID(String filterID) {
		this.filterID = filterID;
	}
	
	/**
     * Function that returns the filter status
     */
	public String getFilterStatus() {
		return(this.filterStatus);
	}
	
	/**
     * Function that sets the filter status
     */
	public void setFilterStatus(String status) {
		this.filterStatus = status;
	}
	
	/**
     * Function that returns the filter name
     */
	public String getFilterName() {
		return(this.filterName);
	}
	
	/**
     * Function that sets the filter name
     */
	public void setFilterName(String filterName) {
		this.filterID = filterName;
	}
	
	/**
     * Function that returns the filter group ID
     */
	public String getFilterGroupID() {
		return(this.filterGroupID);
	}
	
	/**
     * Function that sets the filter group ID
     */
	public void getFilterGroupID(String groupID) {
		this.filterGroupID = groupID;
	}
	
	/**
	 * Function to get the filter fields hash map
	 */
	public HashMap<String, Object> getFilterFields() {
		return(this.filterFields);
	}
	
	/**
	 * Function to set filterFields
	 */
	public void setFilterFields(HashMap<String, Object> fields) {
		this.filterFields.putAll(fields);
	}
	
	/**
     * Function to create a set of filter fields for a tap. The required fields are passed
     * via a HashMap
     */
	public Filter initializeFilter(String filterID, HashMap<String, Object> newFilterFields) {
		// Create the filterID using the information, check if it exists in the filter table and set the fields
		this.filterID = filterID;
		logger.info("Filter exists without fields. Copying fields into filter");
		this.filterFields.putAll(newFilterFields);
		return(this);
	}
	
	/**
     * Function that removes a specified set of fields from a filter
     * 
     */
	public Set<String> removeFilterFields(Set<String> fieldSet) {
		/* A list of the fields that could not be removed is returned. If null is returned, it means
			that an empty set was passed in the first place. If an empty set is returned, all fields
			were successfully removed */
		Set<String> nonRemovableFields = new HashSet<String>();
		if(fieldSet.isEmpty()) {
			logger.info("Empty field set passed. Nothing to do");
			return(null);
		}
		else {
			for(String iter : fieldSet) {
				if(this.filterFields.remove(iter) == null) {
					logger.info("Some fields could not be removed. Check if they exist");
					nonRemovableFields.add(iter);
					continue;
				}
			}
		}
	return(nonRemovableFields);
	}
	
	/**
     * Function to add a source/sink to the filter. Source/sink comprises of a Node, 
     * NodeConnector pair
     */
	public boolean tapAddSrcSinkInfo(HashSet<ImmutablePair<Node, NodeConnector>> newConfig, String type) {
		/*
		 * Symmetric difference of A and B means elements in either set A 
		 * or set B, but not both. In our case,
		 * A is the list of existing sources/sinks and B is the new list, including new additions. So sym(A,B)
		 * will give us the new additions that were made to the list
		 */
		//TODO - Check if the source or the sink already exist in the other lists. There is no point adding the same port
		// as a source and a sink.
		try {
			if(type.equals("sources")) {
				logger.info("New <Node, NodeConnector> source list {} for filter {}", newConfig.toString(), this.filterID);
				this.filterFields.put("sources", newConfig);
			}
			else if(type.equals("sinks")) {
				logger.info("New <Node, NodeConnector> sink list {} for filter {}", newConfig.toString(), this.filterID);
				this.filterFields.put("sinks", newConfig);
			}
			else {
				logger.warn("Unknown destination type specified. Not able to add node(connector)");
				return(false);
			}
		} catch (ClassCastException cce) {
			logger.error("Could not add pair to filter source list, check argument class types."
							+ cce.getMessage());
			return(false);
		} catch (NullPointerException npe) {
			logger.error("Node or NodeConnector is null " + npe.getMessage());
			return(false);
		} catch (IllegalArgumentException iae) {
			logger.error("Problem with Node or NodeConnector structure. "
					+ iae.getMessage());
			return(false);
		}
		return(true);
	}
	
	/**
     * Function to remove a source/sink to the filter. 
     * Source/sink comprise of a Node, NodeConnector pair
     */
	@SuppressWarnings("unchecked")
	public boolean tapRemoveSrcSinkInfo(HashSet<ImmutablePair<Node, NodeConnector>> newConfig, String type) {
		/*
		 * Difference of A and B means elements in set A but not set B. In our case,
		 * A is the list of existing sources/sinks and B is the new list, minus the deletions. So diff(A,B)
		 * will give us the elements that need to be removed from the list
		 */
		try {
			if(type.equals("sources")) {
				logger.info("New <Node, NodeConnector> list {} for filter {} ", newConfig.toString(), this.filterID);
				for(ImmutablePair<Node, NodeConnector> srcEntry : newConfig) {
					if(((HashSet<ImmutablePair<Node,NodeConnector>>) 
							this.filterFields.get("sources")).contains(srcEntry)) {
						this.filterFields.put("sources", ((HashSet<ImmutablePair<Node,NodeConnector>>) 
															this.filterFields.get("sources")).remove(srcEntry));
					}
					else {
						logger.warn("Deletion attempted with non-existent source");
					}
				}
			}
			else if(type.equals("sinks")) {
				logger.info("New <Node, NodeConnector> list {} for filter {} ", newConfig.toString(), this.filterID);
				for(ImmutablePair<Node, NodeConnector> sinkEntry : newConfig) {
					if(((HashSet<ImmutablePair<Node,NodeConnector>>) 
							this.filterFields.get("sinks")).contains(sinkEntry)) {
						this.filterFields.put("sinks", ((HashSet<ImmutablePair<Node,NodeConnector>>) 
															this.filterFields.get("sinks")).remove(sinkEntry));
				}
					else {
						logger.warn("Deletion attempted with non-existent sink");
					}
				}
			}
			else {
				logger.warn("Unknown destination type specified. Not able to remove node(connector)");
				return(false);
			}
		} catch (ClassCastException cce) {
			logger.error("Could not add pair to filter source list, check argument class types."
							+ cce.getMessage());
			return(false);
		} catch (NullPointerException npe) {
			logger.error("Node or NodeConnector is null " + npe.getMessage());
			return(false);
		}
		return(true);
	}
}
