package org.sdnhub.tapapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.utils.Status;
import org.sdnhub.tapapp.internal.Filter;
import org.sdnhub.tapapp.internal.FilterFlowStructure;

public interface ITapManager {
	public UUID createData(TapManagerData datum);

	public TapManagerData readData(UUID uuid);

	public Map<UUID, TapManagerData> readData();

	public Status updateData(UUID uuid, TapManagerData data);

	public Status deleteData(UUID uuid);

	public boolean createFilter(String filterID,
			HashMap<String, Object> newFilterFields);

	public boolean isFilterCreated(String filterID);

	public ArrayList<Filter> listFilters();
	
	public boolean activateTap(String filterID);

	public boolean deactivateTap(String filterID);

	public boolean tapAddSources(String filterID, 
			HashSet<ImmutablePair<Node, NodeConnector>> srcList);
	
	public boolean tapAddSinks(String filterID, 
			HashSet<ImmutablePair<Node, NodeConnector>> sinkList);

	boolean tapRemoveSources(String filterID,
			HashSet<ImmutablePair<Node, NodeConnector>> sinkList);

	boolean tapRemoveSinks(String filterID,
			HashSet<ImmutablePair<Node, NodeConnector>> sinkList);
	
	HashSet<FilterFlowStructure> tapListFlows(String filterID);
}
